Mon travail de thèse a porté sur deux aspects  de la génomique : \begin{itemize}
  \item D'une part, j'ai créé un outil, HLA-Check, pour tester et améliorer la fiabilité d'outils d'acquisition du HLA
  \item D'autre part, j'ai découvert de nouveaux signaux impliqués dans l'évolution de la co-infection VIH/VHC en réalisant des analyses génomiques sur la cohorte HEPAVIH à l'aide d'outils et procédures existantes.
    %j'ai utilisé des outils et procédures existantes, pour effectuer des analyses génomiques sur la cohorte HEPAVIH, découvrant de nouveaux signaux impliqués dans l'évolution de la co-infection VIH/VHC
\end{itemize}

\section{L'imputation du HLA}

Dans le contexte du début de ma thèse, l'outil principal de génotypage des génomes était la puce de génotypage, utilisée pour des études d'association génome entier. Les outils de NGS\cite{goodwin2016coming,}, bien qu'en essor, avaient encore un coût élevé qui ne laissait pas présager la brutale chute des coûts observée en 2015 (figure~\ref{fig-ccl-cost}). En 2017, Illumina a également annoncé une nouvelle plateforme permettant à court terme le séquençage du génome pour moins de 100\$ par génome. De plus, la plupart des études sur des cohortes modestes ne permettent d'identifier que des variants communs, qui sont précisément ceux visés par les puces de génotypages.

Ces évolutions technologiques permettent d'envisager des projets de génotypage de plus en plus ambitieux : depuis HAPMAP (environ 200 individus de 4 populations d'origines ethniques différentes) et 1000Genomes (un millier d'individus de multiples origines ethniques), le NHS (National Health Service, système de santé publique anglais) travaille désormais sur le \og{} 100 000 genomes project \fg{} prévoyant le séquençage de cent mille génomes, uniquement à l'échelle du Royaume-Uni.

\begin{figure}[h!]
  \center\includegraphics[width=\textwidth]{costpergenome2015_4}
  \caption{Coût du séquençage d'un génome dans le temps}
  \label{fig-ccl-cost}
\end{figure}

En raison de son polymorphisme élevé, le CMH reste une zone qui bien qu'importante (détectée notamment dans des GWAS du laboratoire GBA sur le VIH), est difficile à génotyper. Les outils d'imputation du HLA à partir de données SNP, tels que SNP2HLA, très récent, sont une piste prometteuse pour la détection des allèles HLA dans les GWAS.



Dans un premier temps, j'ai testé et étudié les différents outils d'imputation du HLA, et essayé d'améliorer l'imputation proposée par SNP2HLA en utilisant les logiciels développés par le laboratoire, supposément plus performants, en l'occurrence Shape-IT\cite{delaneau2013improved,} et IMPUTE2\cite{howie2012fast,} à la place de beagle\cite{browning2007rapid,}. Les résultats obtenus ont été similaires à ceux d'origine, mais pas significativement supérieurs. 

Après une recherche plus approfondie, nous en avons déduit que l'amélioration apportée par Shape-IT -- environ une inversion de mieux toutes les quelques centaines de milliers de paires de bases -- correspond à des effets observables sur de grandes régions, tandis que notre étude se concentrait sur quelques dizaines de SNPs (les gènes étudiés ne font que quelques centaines ou milliers de bases) répartis sur une petite région (le MHC ne fait qu'environ 4Mb). Ainsi, cette différence d'échelle explique que l'on n'observe pas de différence significative, les algorithmes employés étant globalement équivalents par ailleurs. 

J'ai également étudié le logiciel HIBAG\cite{zheng2014hibag,}, qui utilise des méthodes d'\emph{attribute bagging} à la place des modèles de Markov utilisés dans les autres logiciels : il construit un modèle en testant des ensembles de SNPs statistiquement associés aux paires d'allèles HLA dans le panel de test, ne sélectionnant que quelques milliers d'ensembles de SNPs. Pour déterminer le HLA, les ensembles de SNPs sont testés puis calculent à la majorité la paire d'allèles la plus associée au génotype.

Cette méthode rend la construction du modèle très lente (il faut tester de très nombreux ensembles de SNPs, sélectionnés aléatoirement), mais en contrepartie le typage d'un individu est quasi immédiat. De plus, la forme du logiciel (un paquet R) le rend beaucoup plus simple à utiliser que les autres outils. Du point de vue de la précision, HIBAG donne des résultats environ aussi précis que ceux obtenus avec les autres outils.

Observant qu'à partir des mêmes sources de données, les divers outils d'imputation arrivaient à des résultats similaires, j'ai tenté d'améliorer l'imputation en intégrant diverses sources de données, hétérogènes, détaillées dans la table~\ref{tbl-ccl-scd} : là où un outil d'imputation utilise des SNPs de panels de référence et de test pour déterminer les allèles HLA, HLA-Check utilisera non seulement les SNPs du panel (imputés, donc possédant de l'information du panel de référence d'imputation) et les types HLA à juger, mais aussi les alignements de séquence génomique des allèles HLA connus. 

\begin{table}[h!]
  \center\begin{tabular}{|c|c|c|} \hline
    Méthode & Imputation HLA & HLA-Check \\ \hline
    Entrée & Panel de référence (SNPs) & Typages HLA \\
    & Panel de test (SNPs) & SNPs des exons (imputés) \\
    & & Alignements HLA \\ \hline
    Sortie & HLA imputés & Mesure de confiance dans les HLA \\ \hline
  \end{tabular}
  \caption{Imputation et HLA-Check : types de données}
  \label{tbl-ccl-scd}
\end{table}

J'ai à cet effet utilisé une métrique simple de distance entre une paire d'allèles de gènes et un ensemble de SNPs imputés sur les exons de ces gènes : Pour chaque locus, on prend la probabilité donnée par l'imputation que la paire de nucléotides à ce SNP ne corresponde pas à la paire d'allèles de gène considérée, puis on additionne ces probabilités pour tous les loci disponibles pour obtenir cette distance. 

Si d'un point de vue purement mathématique, cette solution n'apparaît pas idéale car elle ne permet pas d'obtenir la probabilité que la paire d'allèles corresponde au génotype, la métrique alternative, l'addition des logarithmes des probabilités (pour avoir la probabilité combinée) ne prend, elle, pas en compte d'une part la non-indépendance des probabilités générées par les outils d'imputation (au contraire, d'ailleurs, la plupart des SNPs proches imputés seraient très dépendants), et d'autre part donnerait un poids énorme à une erreur d'imputation sur un seul locus. 


Cette approche a permis d'améliorer la fiabilité de l'imputation HLA en isolant les typages qui, selon notre métrique, apparaissent les moins fiables, et donc de diminuer d'un facteur deux environ la proportion d'individus mal imputés, sans éliminer un nombre rédhibitoire de sujets (moins de 5\% en général).

\subsubsection{Perspectives}

Notre outil permet d'utiliser des grandes cohortes déjà génotypées par le passé pour effectuer des analyses sur le HLA de manière plus précises en utilisant l'imputation : un typage HLA coûte aujourd'hui de l'ordre de la centaine d'euros, donc éviter le typage d'une cohorte de milliers de patients en utilisant l'imputation peut permettre d'économiser des centaines de milliers d'euros.

D'ores et déjà, une équipe de recherche chinoise nous a contacté pour demander des précisions sur l'utilisation du logiciel dans l'une de leurs études, ainsi que de l'aide pour en interpréter les résultats. 

De plus, l'approche utilisée dans HLA-Check n'est pas spécifique au HLA : la méthodologie employée n'a besoin que de SNP annotés, et d'alignements relatifs d'allèles connus d'un gène, aussi elle est applicable à tous les autres gènes très polymorphiques du génome, par exemples les gènes MICA, ou KIR. Le logiciel lui-même n'aura besoin que de très mineures adaptations pour les prendre en compte. De plus, le logiciel étant open source, n'importe quel acteur de la communauté est libre d'y intégrer de nouveaux gènes à traiter ou SNPs utilisés pour ses propres besoins, ou même d'utiliser une autre métrique de distance entre allèles.
\vspace{1cm}

En outre, après une GWAS, l'une des études faites dans les régions présentant un intérêt particulier, ou dans les où l'on a trouvé des associations, est une étude dite sur les \emph{haplotypes}. 
Dans ces études, on s'intéresse aux effets combinés des SNPs que l'on détermine présents sur le même brin du génome, pour savoir si on peut associer non pas un unique variant d'un SNP au phénotype, mais une combinaison de quelques SNPs, qui produiraient, quand ils sont présents sur le même brin d'ADN, un effet phénotypique fort. 
Or, lors de ces études, l'imprécision de l'imputation et l'imprécision du phasage provoquent souvent la présence d'haplotypes faux, qui quand on considère par ailleurs le grand nombre d'haplotypes possibles ($2^{n_{\text{SNP}}}$), peut rendre la réalisation des tests statistiques peu fiables.

Dans cette optique, notre approche peut se voir comme une généralisation cette analyse sur les gènes HLA entiers : nous cherchons à étudier les haplotypes HLA dans leur globalité, mais au lieu de considérer toutes les combinaisons possibles de tous les SNPs, on n'examine que les allèles HLA que l'on sait exister au niveau biologique, ce qui simplifie considérablement les configurations à tester, et notre outil permet de rapidement détecter les haplotypes les moins sûrs. 

Il pourrait donc être très utile, dans le cadre des analyses de GWAS, d'étendre notre outil à tous les gènes. Le principal obstacle à cette extension est l'inexistence d'une base de données exhaustive des allèles connus des gènes, si possible alignés, tel qu'il existe pour les gènes du HLA.

À plus long terme, en revanche, les méthodes d'acquisition ayant tendance à évoluer vers le séquençage nouvelle génération, les données séquencées devraient être de plus en plus fiables et fournir des haplotypes sûrs de plus en plus longs, et l'utilité de HLA-Check devrait décroître.












%Quoi fait, pourquoi, contexte, en quoi améliore, impact, utilité, perspectives, coûts
\section{Analyses génomiques de coinfection VIH-VHC}

Les études génome entier sont utilisées depuis environ 2008, et ont connu un grand succès. Les raisons sont multiples : d'une part, la démocratisation des coûts de génotypage a permis à de nombreuses équipes de recherche dans le monde entier de génotyper des population (les puces de génotypage permettent maintenant de génotyper rapidement jusqu'à 5 millions de SNPs), et les GWAS, par leur caractère exhaustif \textit{sans a priori} biologique et la relative simplicité des outils permettant de les effectuer (les protocoles de GWAS sont désormais établis, avec des pipelines d'analyse robustes, parfois automatisés dans les cas les plus simples) comme de leurs fondations statistiques, ont permis de les appliquer dans de nombreux domaines (Figure~\ref{fig-ccl-chronogwas}). 

%Les puces de génotypage permettent maintenant de génotyper rapidement jusqu'à 5 millions de SNPs. Les protocoles de GWAS sont désormais établis avec des protocoles expérimentaux bien établis, avec des pipelines d'analyse robustes, parfois automatisés dans les cas les plus simples. De multiples GWAS sur un même phénotype dans des populations différentes peuvent également donner lieu à des méta-analyses, agrégeant les résultats de différentes études pour augmenter la puissance statistique et donc pouvoir détecter des signaux plus faibles en diminuant le bruit statistique. 

%Étant donné le nombre de tests statistiques effectués, la correction effectuée (Bonferroni, ou le seuil de significativité génome-entier) ne permet en général pas d'observer des signaux réels de variants génétiques, mais à impact biologique limité, à moins de disposer de très grandes cohortes.

\begin{figure}[h!]
  \center\includegraphics[width=\textwidth]{10y}
  \caption{Chronologie des découvertes SNP/phénotype par études génome entier}
  Graphique tiré de \cite{visscher201710} : données du GWAS Catalog\cite{welter2013nhgri}, comptant uniquement les SNPs à $5.10^{-8}$, en enlevant les SNPs en $r^2>0.5$
  \label{fig-ccl-chronogwas}
\end{figure}

\begin{figure}[h!]
  \center\includegraphics[width=\textwidth]{oddsfreq}
  \caption{Faisabilité de l'identification de variants génétiques par fréquence allélique et force de l'effet génétique (OR)}
  Plus l'effet est faible, plus la fréquence allélique doit être forte pour détecter l'association, à taille de cohorte égale par ailleurs
  Graphique tiré de \cite{manolio2009finding,}
\end{figure}

%Une approche alternative serait de généraliser les études s'appuyant sur le ``taux de fausses découvertes'' (\textit{False Discovery Rate}) des résultats, qui est une approche moins stricte permettant d'isoler en général davantage de signaux, tout en étant conscient du taux attendu de faux signaux trouvés.

\subsubsection{L'étude HEPAVIH}

Les mécanismes de la co-infection VIH/VHC étant encore mal compris, et l'équipe GBA ayant une expertise dans les études génomes entier sur les cohortes de maladies, et tout particulièrement sur le VIH, l'équipe a commencé à étudier la cohorte HEPAVIH en collaboration avec l'ANRS dès 2013. Cette collaboration, menée initialement par Damien Ulveling, a donné lieu à une publication\cite{ulveling2016new,} sur la progression de la fibrose chez les patients co-infectés.

Notre étude s'est davantage orientée sur la fibrose et le passage à la cirrhose : là où \cite{ulveling2016new,} cherche à étudier la progression tout au long du phénotype, le passage du METAVIR 3(fibrosé, mais non cirrhotique) au METAVIR 4 (cirrhotique), seule étape du processus considérée non réversible, et très variable selon les individus : certains patients passeront rapidement en état cirrhotique, d'autre non.

Nous avons donc procédé à une étude d'association génome entier chez des patients co-infectés VIH/VHC en isolant les patients avec un score METAVIR extrême (une cirrhose), contre ceux qui ont un score METAVIR faible (0 à 2), et l'étude a permis d'isoler trois signaux inédits détaillés ci-après. Deux d'entre eux sont connus pour jouer a priori un rôle biologique dans le développement de la fibrose ou le cancer du foie. Si nous aurions aimé pouvoir étudier les phénotypes des patients ayant les réponses les plus extrêmes à l'infection comme un cancer ou une cirrhose rapide (en effet, les études sur les phénotypes extrêmes, comme \cite{le2009genomewide,} peuvent aider à identifier les allèles les plus impliqués dans un phénotype), leur décès précoce a empêché tout génotypage.


Un des intérêts de la cohorte HEPAVIH est le grand nombre de caractéristiques phénotypiques suivies au cours du temps chez ses membres. Il est donc envisageable d'effectuer d'autres études d'association sur divers phénotypes. En particulier, le laboratoire compte étudier la réponse au traitement du VIH et l'évolution de la charge virale : on pourra ainsi déterminer d'éventuels paramètres génétiques pouvant déterminer pour un individu le traitement le plus susceptible d'être efficace. De plus, nous allons également profiter sous peu d'une mise à jour des informations sur la cohorte permettant d'observer l'évolution des patients sur les dernières années.

%Les mécanismes trouvés sont a priori indépendants de la réponse au virus : l'étude répliquée chez les patients n'ayant pas fait de réponse au VHC a donné des résultats similaires. 

\subsubsection{Résultats}

Le signal le plus important de cette GWAS est situé dans le gène \gene{CTNND2} sur le chromosome 5, avec cinq variants introniques. Ce gène a déjà été associé dans des études sur la formation de cancers\cite{lu2010delta,}, la protéine qu'il code ($\delta$-caténine) étant impliquée dans les mécanismes de contact entre cellules\cite{kosik2005delta,}. Notamment, cette protéine interagit directement avec la p120-caténine et la E-cadhérine, deux protéines dont les niveaux d'expression ont été corrélé avec les tumeurs du foie, et la métastase du cancer du foie, ainsi qu'avec le taux de survie des patients.

Le deuxième signal découvert, dans le chromosome 1, semble n'avoir aucun lien avec un gène ni fonction biologique : le gène le plus proche se trouve à plus de 500kb. Les diverses hypothèses pour sa détection sont la possibilité d'un faux positif (la probabilité de faux positif ayant beau être garantie de moins de 5\%, elle n'est jamais nulle, et tout signal que l'on trouve dans une étude est toujours susceptible d'être un faux positif); 
mais ce signal peut aussi être révélateur d'un haplotype (si ce SNP est associé à une combinaison de deux allèles de deux gènes alentour, par exemple, sans que l'un ou l'autre, individuellement, ne soient associés au phénotype); ou encore correspondre à la possibilité de nouveaux mécanismes du génome non encore découverts dans cette région. 

Le dernier signal est situé dans le gène \gene{MIR7-3HG}, un gène ne codant pas de protéine, mais contenant le micro-ARN MIR-7, qui a déjà été identifié dans des études sur le carcinome hépatocellulaire\cite{wang2016microrna}. 

Bien que la région HLA est reliée à certains mécanismes d'action du VIH et du VHC, après avoir tenté d'imputer les allèles HLA, nous n'avons pas pu mettre en évidence d'associations entre un allèle du HLA et le phénotype testé.


Dans une approche exploratoire, cherchant à découvrir des signaux pertinents tout en contrôlant le taux de signaux faux positifs, j'ai également effectué des études de FDR (\textit{false discovery rate}) avec un seuil de 15\% (en espérance, on s'attend donc à 15\% de faux positifs), avec 8 signaux détectés proches de gènes, dont plusieurs semblent avoir un lien biologique avec le fonctionnement du foie (\gene{MIR7-3HG, KIRREL, FMO11P, CTNND2, STAU2, GYS-1, NRIP-1, LOC105375988}).

Nous avons ensuite contacté une autre cohorte, comportant des patients infectés par le VHC, pour tenter de répliquer ces résultats. L'un de nos signaux, rs138336017, a été répliqué dans la cohorte GENOSCAN avec une p-valeur de $0.03$. Ce SNP étant imputé et non génotypé dans nos données, des études du coût de son génotypage ont été entreprises au laboratoire afin de déterminer précisément son degré d'association.


Enfin, nous avons effectué une étude de voies de signalisation, ou \emph{pathways}\cite{ramanan2012pathway,}. Ces études visent, par exemple à partir des résultats d'une étude génome entier, à déterminer si une voie de signalisation moléculaire est associée au phénotype étudié. On y considère des \textit{ensembles de gènes} avec des points communs, par exemple d'être impliqué dans un processus biologique donné, voire une maladie. On regarde alors si les p-valeurs des SNPs dans cet ensemble de gène diffèrent significativement de celles attendues sous l'hypothèse nulle, pour en extraire la p-valeur de l'ensemble de gènes. Enfin, on détermine si la p-valeur est significative, classiquement (avec une correction statistique si on regarde plusieurs ensembles de gènes, par exemple lorsque l'on teste des bases de données comme KEGG\cite{kanehisa2009kegg,}, les pathways de Gene Ontology\cite{ashburner2000gene,}, ou Reactome\cite{croft2010reactome,}). 

\begin{figure}[h!]
  \center\includegraphics[width=\textwidth]{nihms368356}
  \caption{Les principaux types de pathways}(Tiré de \cite{ramanan2012pathway,})
  %Les principaux types de pathways et leurs relations \\ %(b) Pathways et réseaux sont complémentaires : Les pathways sont organisés de manière fonctionnelle vers un but (une fonction), tandis que les éléments de réseaux sont connextés par des points communs qui peuvent ne pas correspondre à des actions, et doivent être pris en compte dans leur ensemble.
\end{figure}

J'ai utilisé à cette fin l'outil GSEA\cite{subramanian2005gene,}. Les résultats obtenus semblent comporter beaucoup de faux positifs, mais il est intéressant de constater qu'un des pathways les plus significatifs est Reactome:Amyloids, un pathway associé à la formation de protéines fibreuses, et donc potentiellement très pertinent pour la fibrose hépatique.



\subsubsection{Perspectives}

Afin d'établir ou d'infirmer nos signaux, il serait intéressant de chercher à les répliquer dans d'autres cohortes de patients infectés par le VHC.
Idéalement, il faudrait tenter de répliquer nos résultats dans d'autres cohortes composées de patients co-infectés par le VIH et VHC, mais en leur absence, après la réplication GENOSCAN,
la cohorte utilisée par Urabe\cite{urabe2013genome} qui s'intéresse également à des phénotypes de fibrose chez des patients monoinfectés, serait un bon candidat pour une seconde réplication. 


L'approche par voies de signalisation permet d'identifier des mécanismes biologiques fortement impliqués dans le phénotype : y compris quand les effets des composantes individuelles des éléments de la voie de signalisation sont faibles, une combinaison d'effets convergents peut se détecter par une telle approche. Nous avons effectué une première analyse, mais il serait intéressant de tester d'autres ensembles de pathways, ainsi que d'autres outils pour les comparer avec le résultat obtenu par GSEA.

\nomenclature{NGS}{Next Generation Sequencing}
Enfin, il est possible que des variants rares soient impliqués dans le déclenchement de la cirrhose. Pour déterminer les variants rares, il serait nécessaire d'effectuer un nouveau génotypage par NGS, mais les coûts actuels liés aux NGS rendent peu probable cette analyse à l'échelle de la cohorte à courte échéance.

\vspace{1cm}

Si un traitement pour l'hépatite existe déjà, et les patients sont en général traités et guéris, ces nouvelles études génomiques peuvent aider sur plusieurs points : ils peuvent permettre de trouver des traitements complémentaires, soit plus efficaces (en général, ou seulement sur certaines catégories de patients), soit ayant moins d'effets secondaires, d'analyser d'éventuelles résistances à un traitement, ou encore de trouver des gènes déterminants dans une régression de l'état du patient, et potentiellement importants dans son suivi.

En effet, si nous pouvons détecter les variants qui favorisent le plus une évolution rapide de l'état du foie passant d'une fibrose réversible, à une cirrhose irréversible, il sera d'autant plus simple de pouvoir déterminer pour un patient s'il aura besoin d'un suivi plus particulier au cours de son traitement. 

Ces techniques de médecine personnalisée, qui visent à adapter à chacun son suivi et son traitement médical en fonction notamment de ses déterminants génétiques (mais aussi environnementaux), si elles ne sont encore que marginalement utilisées, pourraient être amenées à se généraliser dans le futur en raison de leur intérêt thérapeutique (pour mieux traiter les patients) et financier (pour orienter au mieux le dépistage afin de détecter aussi tôt que possible l'apparition de symptômes chez des patients identifiés comme à risque).%, en dépit des risques éthiques qu'elles comportent (par exemple, en cas de mésusage du concept par des assurances privées).


De plus, il existe également une probabilité de régression : certaines personnes en apparence guéries peuvent au bout d'un laps de temps avoir à nouveau une fibrose hépatique, voire une cirrhose. Toujours dans l'optique de se rendre en mesure d'adapter le suivi des patients (qui peut parfois être contraignant pour ceux-ci) aux risques qu'ils présentent de devoir reprendre un traitement contre le VHC, il serait intéressant d'effectuer des études sur la régression de la fibrose.
On pourra ainsi également comparer ces résultats à ceux obtenus pour l'évolution de celle-ci, afin de déterminer la présence d'éventuelles causes communes de ces procédés.




