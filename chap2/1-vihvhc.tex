\section{Le VIH}

\nomenclature{VIH}{Virus de l'Immunodéficience Humaine}
\nomenclature{SIDA}{Syndrome d'ImmunoDéficience Acquise}
\nomenclature{ONU}{Organisation des Nations Unies}

\subsection{La maladie}

Le VIH, pour Virus de l'Immunodéficience Humaine, est un rétrovirus affectant le système immunitaire humain, et est responsable du syndrome d'immunodéficience acquise (SIDA). Il a été découvert en 1983\cite{barre1983isolation,} et l'infection cible en priorité les lymphocytes T CD4\cite{klatzmann1984t,zagury1986long,}, principaux coordinateurs de l'immunité. En conséquence, l'infection aboutit à la destruction du système immunitaire, perturbé par la colonisation virale\cite{coffin1995hiv,}.
L'épidémie de SIDA, depuis le début des années 80, s'est étendue au monde entier, et reste un des problèmes majeurs de santé publique dans le monde, avec notamment un comité de l'ONU dédié au SIDA (ONUSIDA\cite{unaids}) et recensant l'épidémie\cite{un2017aidsinfo}.

%\subsection{Mode d'infection}
Les trois méthodes de transmissions principales du VIH sont la voie sexuelle (principale voie de contamination), la voie sanguine, et la voie verticale (mère-enfant). 

\subsection{Évolution de l'infection}

La progression de l'infection est caractérisée par deux facteurs : la charge virale (quantité de virus), et la quantité de lymphocytes T CD4+ dans le sang. Trois phases composent l'infection (Figure~\ref{fig-phases-vih}) : 
\begin{itemize}
  \item la primo-infection, généralement sans effet caractéristique particulier, avec une chute brutale de la quantité de lymphocytes et une forte augmentation de la charge virale;
  \item la phase de latence est une phase où l'activité du virus et la réponse immunitaire se compensent : il n'y a pas d'effets phénotypiques, et la diminution du taux de CD4+ comme l'augmentation de la charge virale sont très faibles. Cette phase peut durer plusieurs années sans traitement.
  \item la phase de SIDA est caractérisée par l'arrivée de maladies opportunistes, prenant avantage de la chute du taux de lymphocytes T CD4+. 
\end{itemize}

%\subsection{SIDA}
\begin{figure}[h]
  \center\includegraphics[width=0.5\textwidth]{PhasesVIH}
    \caption{Phases du VIH}La phase aiguë correspond à un pic de charge virale et une chute du taux de CD4+, et est suivie d'une longue phase asymptomatique durant laquelle le taux de CD4+ diminue lentement, avant l'entrée en phase SIDA, où la charge virale progresse à nouveau et où le malade devient vulnérable aux maladies opportunistes(tiré de \cite{alizon2012modelling,}).
  \label{fig-phases-vih}
\end{figure}

\subsection{Physiopathogenèse de l'infection par le VIH-1}
\subsubsection{Cycle de réplication}
Pour ralentir ou contrer l'infection par le VIH, ou pour prolonger la phase de latence (asymptomatique) avant la phase SIDA, les recherches se sont concentrées sur les mécanismes d'action et de réplication du virus\cite{freed2001hiv}. La figure~\ref{fig-phases-rep-vih} détaille les étapes du cycle de réplication du virus avec les protéines humaines impliquées à chaque étape.

\subsubsection{Hypothèses de physiopathogenèse}

L'infection par le VIH entraîne une un déficit de l'immunité cellulaire causé en grande partie par la baisse de la population de lymphocytes T CD4+, mais également par d'autres mécanismes pas encore élucidés. 

De manière directe, on peut expliquer en partie la baisse du taux de CD4+ par une lyse (destruction de la cellule causée par l'attaque de sa membrane) directe des cellules infectées. Cependant, même chez des sujets affectés par le SIDA, seuls environ 10\% des CD4+ exprimant le génome viral, ce qui ne suffit pas à expliquer la déplétion observée\cite{brinchmann1991few,}.

Indirectement, plusieurs hypothèses existent pour expliquer la baisse du taux de CD4+ : \begin{itemize}
  \item Une réponse spécifique contre le VIH pourrait affecter les CD4+ adsorbant les antigènes des cellules infectées\cite{walker1987hiv,}
  \item Des superantigènes viraux pourraient activer les lymphocytes T conduisant à l'apoptose (autodestruction) des CD4+\cite{estaquier1994programmed,}
  \item Certaines protéines virales diminueraient la présence des HLA de classe I à la surface des cellules infectées, les protégeant de la détection par le système immunitaire\cite{greenberg1998sh3,}
  \item La présence du virus pourrait déséquilibrer la proportion des CD4+ ayant les fonctions Th1 et Th2, cet équilibre étant important pour une réponse immunitaire adaptée\cite{clerici1993th1,}
  \item L'attaque spécifique du VIH dans les muqueuses gastro-intestinales peut déclencher la présence dans le sang de bactéries capables de maintenir le système immunitaire en hyperactivité chronique pendant la durée de l'infection\cite{marchetti2013microbial,}
\end{itemize}

Ces hypothèses sont non-exclusives, et font partie des pistes envisagées pour élucider les mécanismes d'action du VIH. 


\begin{figure}[h]
  \center\includegraphics[width=\textwidth]{barrsinoussi2013_p3}
  \caption{Cycle de réplication du VIH}
  (Tiré de \cite{barre2013past} )

  \label{fig-phases-rep-vih}
  Cette figure illustre les principales étapes du cycle de réplication du VIH-1: la liaison aux récepteurs et co-récepteurs du CD4, la fusion avec la membrane, la libération de l'ARN, la transcription inverse de l'ARN du VIH dans l'ADN, puis la translocation dans le noyau. Une fois l'ADN viral intégré à l'ADN hôte, la transcription permet de recréer de l'ARN viral et des protéines, qui viendront se placer à la surface de la cellule pour former des virions immatures. Enfin, ces virions sont relâchés, maturent, et iront infecter d'autres cellules. 
  
  Le diagramme indique également les voies d'action des grandes familles d'antirétroviraux (en vert), et les facteurs clés de restriction du VIH avec leurs antagonistes viraux.

\end{figure}

\subsection{Études d'association génétique dans le SIDA}
Un grand nombre d'études génétiques ont été menées sur la réponse de l'hôte à l'infection par le VIH, pour aider à comprendre les mécanismes moléculaires mis en jeu dans l'interaction virus-hôte, notamment la déplétion observée du système immunitaire, des études ``gènes candidats''\cite{an2010host,o2013host} aux études ``génome entier'' depuis 2007\cite{fellay2007whole}.

\nomenclature{MAF}{Fréquence de l'Allèle Mineur}
Plusieurs études génome entier ont été menées, entre autres dans le laboratoire GBA, par analyse de la cohorte GRIV composée de patients à profils de progression extrêmes vers le SIDA (comparant des patients progressant très lentement dans la phase de latence ou \og{} non progresseurs\fg{}, contre des patients à la phase de latence très courte, ou \og{} progresseurs rapides \fg{})\cite{hendel1996contribution,hendel2001validation,}. Ces études génome entier se sont appuyées sur des approches statistiques mais aussi bioinformatiques (MAF faibles, eQTL, etc.) et ont révélé plusieurs gènes potentiellement impliqués dans le développement de la maladie, comme HLA*B57, déjà connu, mais aussi \gene{CXCR6}, \gene{RICH2}, \gene{PRMT6}, \gene{SOX5}, ou \gene{TGFBRAP1}\cite{limou2009genomewide,le2009genomewide,limou2010multiple,le2011screening,spadoni2015identification,}.

\section{Le VHC}

\subsection{L'hépatite C}

\nomenclature{VHC}{Virus de l'Hépatite C}
L'hépatite C est une maladie du foie causée par un virus. Le VHC (virus de l'hépatite C) est un virus à ARN du genre Hepacivirus de la famille des Flaviviridae, et a été découvert en 1989 après plusieurs années de recherche pour un virus jusque là qualifié d'hépatite ``non A non B''.
Du fait de l'absence de contrôles spécifiques, ce virus a notamment contaminé jusqu'à 90\% des hémophiles transfusés pré-1990, et on estime dans le monde à plus de 170 millions le nombre de personnes infectées, avec d'importantes variations suivant les pays\cite{mohd2013global}, et différents génotypes du virus ont été observés, à des proportions variant également géographiquement (Figure~\ref{fig-vhc-carte}).

\begin{figure}[h]
  \center\includegraphics[width=\textwidth]{hep27259-fig-0001-m}
  \caption{ Prévalence relative des différents génotypes du VHC par région. } 
  \label{fig-vhc-carte}
  La taille des diagrammes est proportionnelle à la prévalence du virus dans la région.
  (D'après \cite{messina2015global})
\end{figure}


Le virus se transmet essentiellement par le sang, que ce soit par transfusion ou injections (par exemple de drogues) avec des seringues contaminées : On estime de 80\% des toxicomanes sont affectés par le virus\cite{van1990prevalence,}.


%\subsection{Mécanismes pathogéniques}
\subsection{Évolution de l'infection}

Le virus du VHC infecte principalement les cellules hépatiques, où l'on peut retrouver plusieurs millions de copies d'ARN viral par gramme de tissu infecté, alors qu'il est peu détecté dans le reste du corps. Le virus est responsable à la fois de l'infection aiguë et de l'infection chronique. La forme aiguë de l'infection est dans la majorité des cas asymptomatique. Dans 20 à 50\% des cas, les patients vont éliminer le virus, et dans 50 à 80\% des cas, les individus avec une hépatite aiguë vont évoluer vers une hépatite chronique. L'infection chronique est caractérisée par la présence en continu de  l'ARN du VHC dans le foie. L'infection chronique va entraîner des lésions au niveau du foie qui vont mener à la fibrose des tissus hépatiques, qui pourra mener dans un tiers des cas à la cirrhose, qui peut à son tour évoluer vers un hépatocarcinome cellulaire (HCC). L'évolution de la fibrose est aussi généralement asymptomatique et la détection d'une cirrhose peut être trop tardive et ne pas permettre de traiter les patients. Pour ces raisons, le suivi de ceux-ci est très important, et le développement d'outils de prédiction génétique pourrait largement contribuer à un meilleure évaluation des patients et à un meilleur suivi. 

Cette évolution de la fibrose est influencée par un nombre de facteurs importants, comme le sexe, l'âge, ou la surconsommation d'alcool.
\subsection{Physiopathogenèse de l'infection par le VHC}
\subsubsection{Cycle de réplication}
La figure~\ref{fig-vhc-rep} présente les principales étapes du cycle du VHC.

\begin{figure}[h!]
  \center\includegraphics{HCV_rep}
  \caption{Cycle du VHC}
  Après son entrée dans la cellule et décapsidation, le VHC traduit ses protéines, se réplique, puis reforme de nouveaux virions.
  \label{fig-vhc-rep}
  \cite{lindenbach2005unravelling}
\end{figure}

%\subsection{Cirrhose et hépatocarcinome}

%L'hépatite fait évoluer le score METAVIR \cite{ishak1995histological} au cours du temps, et si la progression dans la plupart des stades est généralement réversibles, on considère l'arrivée, rare, au stade F4 -- cirrhose-- comme irréversible. la figure~\ref{fig-vhc-evo} présente les évolutions et l'influence des thérapies existantes.

\begin{figure}[h!]
  \center\includegraphics[width=0.8\textwidth]{HCV_devel}
  \caption{Évolution de l'état du foie infecté par le VHC vers l'hépatocarcinome}
  Tiré de \cite{baumert2017hepatitis,}
  \label{fig-vhc-evo}
\end{figure}


\subsubsection{Hypothèses de physiopathogenèse}

Les mécanismes d'action du VHC sont mieux connus que ceux du VIH, et principalement indirects : En effet, le virus du VHC n'entraînant pas la destruction des cellules du foie, un des principaux facteurs de la pathogenèse de l'hépatite C chronique est la réponse immunitaire de l'hôte. On sait notamment que l'infection chronique des cellules hépatiques par le VHC va créer un déséquilibre des messagers de l'immunité (cytokines) qui pourrait aboutir à une réponse immunitaire qualitativement inadaptée à l'élimination du virus, et être à l'origine d'une réaction inflammatoire excessive favorisant l'évolution de la fibrose au cours de l'infection.\cite{fiel2010pathology,sobue2001th1,}.


\subsection{Études génomiques sur le VHC}

Plusieurs GWAS se sont intéréssé à étudier les variants génétiques associés à la progression du VHC : \cite{rauch2010genetic,} pour \gene{IFNL3} et \gene{IFNL4}, \cite{miki2011variation,} pour \gene{DEPDC5}, \cite{kumar2011genome,} pour \gene{MICA}, \cite{patin2012genome,} pour \gene{RNF7}, \gene{MERTK}, et \gene{TULP1}, \cite{urabe2013genome,} pour \gene{C6orf10} et \gene{BTNL2}, \cite{miki2013hla,} pour \gene{HLA-DQ} et \cite{duggal2013genome,} retrouve \gene{IFNL4} et \gene{HLA-DQ}. La figure~\ref{fig-vhc-gen} résume les étapes de la progression associées à chaque gène.

\begin{figure}[h!]
    \center\includegraphics[width=0.8\textwidth]{HCV_genes}
      \caption{Gènes impliqués dans la progression clinique du VHC}
      Les gènes en gras ont été identifiés par GWAS, les autres par approche gènes candidats.
        Tiré de \cite{matsuura2016host}
          \label{fig-vhc-gen}
\end{figure}









