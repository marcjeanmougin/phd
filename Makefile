all: 
	pdflatex these.tex
	pdflatex these.tex
	makeindex these.nlo -s nomencl.ist -o these.nls
	makeindex these.idx
	biber these
	pdflatex these.tex
	pdflatex these.tex
	pdflatex these.tex
